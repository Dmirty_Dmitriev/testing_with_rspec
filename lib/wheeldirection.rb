module WheelDirection
    class Gear
        attr_reader :chainring, :cog
        def initialize(chainring, cog)
            @chainring = chainring
            @cog = cog
        end
        def ratio
            chainring/cog.to_f
        end
        def gear_inches(diameter)
            ratio*diameter
        end
    end
    class Wheel
        attr_reader :rim, :tire, :gear
        def initialize(chainring, cog, rim, tire)
            @rim = rim
            @tire = tire
            @gear = Gear.new(chainring, cog)
            puts gear.ratio
        end
        def diameter
            rim + (tire*2)
        end
        def gear_inches
            gear.gear_inches(diameter)
        end
    end
end
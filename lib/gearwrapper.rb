require "someframework"
module GearWrapper
    def self.gear(args)
        SomeFramework::Gear.new(args[:chainring],
                                args[:cog],
                                args[:wheel])
    end
end
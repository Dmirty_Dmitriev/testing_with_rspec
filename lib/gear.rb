require "wheel"
class Gear
    attr_reader :chainring, :cog, :wheel
    def initialize(hash)
        hash = defaults.merge(hash)
        @chainring = hash[:chainring]
        @cog  = hash[:cog]
        @wheel = hash[:wheel]
        
    end
    def defaults
        {chainring: 40, cog: 18}
    end
    def ratio
        chainring / cog.to_f
    end
    def gear_inches
        ratio * wheel.diameter
    end
    
end

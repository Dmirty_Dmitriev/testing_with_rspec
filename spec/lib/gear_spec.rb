require "spec_helper"
require "gear"
require "wheel"

describe Gear do
    before(:all) do
        @chainring = 50
        @cog = 40
        @rim = 25
        @tire = 2
        @wheel = Wheel.new(@rim,@tire)
        @hash = {chainring: @chainring, cog: @cog, wheel: @wheel }
        @gear = Gear.new(@hash)
    end
    it "counts ratio" do
        @gear.ratio.should == @chainring/@cog.to_f
    end
    it "counts geear_inches" do
        @gear.gear_inches.should == @gear.ratio*(@rim + (@tire*2))
    end
    it "uses default chainring = 40 and cog = 18" do
        Gear.new({wheel:@wheel}).gear_inches.should == (40/(18.to_f))*(@rim + (@tire*2))
    end
    
end
require "spec_helper"
require "wheel"
require "gear"
require "wheeldirection"

describe WheelDirection::Wheel do
    before(:all) do
        @chainring = 52
        @cog = 11
        @rim = 26
        @tire = 1.5
        @wheel = Wheel.new(@rim,@tire)
        @hash = {chainring: @chainring, cog: @cog, wheel: @wheel }
        @gear = Gear.new(@hash)
        @whild_gear = WheelDirection::Gear.new(@chainring,@cog)
    end
   
    it "calculates gear_inches" do
        
        WheelDirection::Wheel.new(
                            @chainring,
                            @cog,
                            @rim,
                            @tire
                            ).gear_inches.should == Gear.new(@hash).gear_inches
    end
end
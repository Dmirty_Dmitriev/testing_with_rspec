require "spec_helper"
require "wheel"
require "gearwrapper"
require "gear"


describe GearWrapper do
    it "sould use hash" do
        @chainring = 52
        @cog = 11
        @rim = 26
        @tire = 1.5
        @wheel = Wheel.new(@rim,@tire)
        @hash = {chainring: @chainring, cog: @cog, wheel: @wheel }
        @gear = Gear.new(@hash)
        GearWrapper.gear(
            :chainring => @chainring,
            :cog => @cog,
            :wheel => Wheel.new(@rim,@tire)
        ).gear_inches.should == Gear.new(@hash).gear_inches
    end
end
require "spec_helper"
require "wheel"

describe Wheel do
    it "counts circumferece" do
        @rim = 26
        @tire = 2.5
        @wheel = Wheel.new(@rim, @tire)
        @wheel.circumference.should == (@rim+(@tire*2)) * Math::PI
    end
end